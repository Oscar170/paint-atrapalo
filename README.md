# Resumen del proyecto

## Distribución de carpetas
#### Cada fichero tiene su test en el mismo directorio
- actions: clases/funciones que cuentan con lógica de negocio.
- containers: agrupación de elementos visuales.
- helpers: clases/funciones auxiliares que no tienen lógica.
- store: clases para guardar el estado de los datos.
- ui: elementos visuales.

## Decisiones tomadas

El store porvera a otras clases el contexto y canvas para poder realizar las operaciones necesarias, por lo que su unica funcionalidad practicamente es guarda las herramientas que cargamos.

Las herramientas cargadas contaran con una interfaz para iniciaizar y eliminar eventos y demás datos necesarios para realizar su funcionamiento.

Para poder separar el comportamiento de la obtención de datos (normalmente coordenadas) se ha decidido crear clases auxiliares a los que poder suscribirnos y poder recuperar los datos parseados sin importar el tipo de evento, por lo que poder hacer que trabaje en un tipo de dispositivos táctiles o no etc. dependerá de crear diferentes implementaciones.

La interfaz ya que actualmente no cuenta con modificaciones de estado ni actualizaciones complejas, he decidido hacerla en nativo. En el caso que esto cambiase seria conveniente cambiar a una libreria para realizar interfaces. Por el tipo de programación funcional lo mas recomendable es actualizar a React.

La creación del canvas esta preparada para que en un futuro podamos añadir otras capas de canvas para poner elementos extras de la interfaz(seleccionar un elemento) y no tener que realizar ese tipo de operaciones en el canvas principal.