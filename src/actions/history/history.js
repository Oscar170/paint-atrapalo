// @flow

export interface History {
  update(imageUrl: string): void;
  undo(): Promise<HTMLImageElement>;
  redo(): Promise<HTMLImageElement>;
}
