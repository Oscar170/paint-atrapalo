// @flow
import type { History } from './history'

export default class HistoryList implements History {
  history = []

  current: number = 0

  update(imageUrl: string): void {
    this.history = [
      ...this.history.slice(0, this.current === null ? 0 : this.current + 1),
      imageUrl,
    ]
    this.current = this.history.length - 1
  }

  undo(): Promise<HTMLImageElement> {
    const current = this.current <= 0 ? 0 : this.current - 1
    this.current = current
    return this.getImage(current)
  }

  redo(): Promise<HTMLImageElement> {
    const current = (this.current + 1) < this.history.length ? this.current + 1 : this.current
    this.current = current
    return this.getImage(current)
  }

  getImage(index: number): Promise<HTMLImageElement> {
    if (this.history.length <= 0) {
      return Promise.reject(new Error('NO_HISTORY'))
    }

    return new Promise((res) => {
      const image = new Image()
      image.src = this.history[index]
      image.onload = () => {
        res(image)
      }
    })
  }
}
