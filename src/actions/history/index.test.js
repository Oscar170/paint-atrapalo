import HistoryList from './index'

const error = new Error('NO_HISTORY')

Object.defineProperty(Image.prototype, 'onload', {
  set: (fn) => {
    fn()
  },
})

test('if the history isn\'t load throw error, when try to undo history', async () => {
  const history = new HistoryList()

  try {
    await history.undo()
  } catch (e) {
    expect(e).toEqual(error)
  }
})

test('if the history isn\'t load throw error, when try to redo history', async () => {
  const history = new HistoryList()

  try {
    await history.redo()
  } catch (e) {
    expect(e).toEqual(error)
  }
})

test('should add new entry to history', () => {
  const history = new HistoryList()
  history.update('a')

  expect(history.history).toEqual(['a'])
})

test('can not get an item before the first record', async () => {
  const history = new HistoryList()
  history.update('a')
  history.update('b')

  const history1 = await history.undo()
  const history2 = await history.undo()
  const history3 = await history.undo()
  const history4 = await history.undo()

  expect(history1.src).toBe('http://localhost/a')
  expect(history2.src).toBe('http://localhost/a')
  expect(history3.src).toBe('http://localhost/a')
  expect(history4.src).toBe('http://localhost/a')
})

test('can not get a newer element to the last record', async () => {
  const history = new HistoryList()
  history.update('a')
  history.update('b')

  const history1 = await history.undo()
  const history2 = await history.redo()
  const history3 = await history.redo()
  const history4 = await history.redo()

  expect(history1.src).toBe('http://localhost/a')
  expect(history2.src).toBe('http://localhost/b')
  expect(history3.src).toBe('http://localhost/b')
  expect(history4.src).toBe('http://localhost/b')
})
