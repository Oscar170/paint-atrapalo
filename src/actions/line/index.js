// @flow
import type { Point } from '../../helpers/point'

// eslint-disable-next-line max-len
const drawLine = (ctx: CanvasRenderingContext2D) => ({ init, finish }: { init: Point, finish: Point }) => {
  ctx.beginPath()
  ctx.moveTo(init.x, init.y)
  ctx.lineTo(finish.x, finish.y)
  ctx.closePath()
  ctx.stroke()
}

export default drawLine
