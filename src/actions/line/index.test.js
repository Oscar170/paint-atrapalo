import drawLine from './index'

const mockContext = {
  beginPath: jest.fn(),
  moveTo: jest.fn(),
  lineTo: jest.fn(),
  closePath: jest.fn(),
  stroke: jest.fn(),
}

test('should return a function', () => {
  const contextDrawLine = drawLine(mockContext)

  expect(typeof contextDrawLine).toBe('function')
})

test('should draw a line', () => {
  drawLine(mockContext)({
    init: { x: 4, y: 7 },
    finish: { x: 5, y: 7 },
  })

  expect(mockContext.beginPath).toBeCalled()
  expect(mockContext.moveTo).toBeCalled()
  expect(mockContext.lineTo).toBeCalled()
  expect(mockContext.closePath).toBeCalled()
  expect(mockContext.stroke).toBeCalled()
})
