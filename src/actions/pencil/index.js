// @flow
import type { ToolLifeCycle } from '../../store/draw'
import type { History as PaintHistory } from '../history/history'
import DragClick from '../../helpers/drag-click'
import drawLine from '../line'

export default class Pencil implements ToolLifeCycle {
  dragClick: DragClick

  contextDrawLine: Function

  updateHistory: Function

  constructor(canvas: HTMLCanvasElement, ctx: CanvasRenderingContext2D, history?: PaintHistory) {
    this.dragClick = new DragClick(canvas)
    this.contextDrawLine = drawLine(ctx)

    if (history) {
      // flow-disable-next-line
      this.updateHistory = () => history.update(canvas.toDataURL())
    }
  }

  onInit(): void {
    this.dragClick.subscribe(this.draw)
    this.dragClick.init()
  }

  onDestroy(): void {
    this.dragClick.destroy()
  }

  draw = ({ type, move }): void => {
    this.contextDrawLine(move)
    if (type === 'finish') {
      if (this.updateHistory) this.updateHistory()
    }
  }
}
