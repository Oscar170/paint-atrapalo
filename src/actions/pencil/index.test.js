import Pencil from './index'

const mockEvent = (x, y) => ({
  stopImmediatePropagation: () => {},
  target: {
    getBoundingClientRect: () => ({ top: 0, left: 0 }),
  },
  pageX: x,
  pageY: y,
})

describe('pencil functionality', () => {
  const mockCanvas = document.createElement('canvas')
  const pencil = new Pencil(mockCanvas, {})

  beforeAll(() => {
    pencil.contextDrawLine = jest.fn()
    pencil.onInit()
  })

  afterAll(() => {
    pencil.onDestroy()
  })

  test('on click/drag on the canvas should call draw line', () => {
    mockCanvas.dispatchEvent(new CustomEvent('mousedown', mockEvent(10, 10)))
    mockCanvas.dispatchEvent(new CustomEvent('mousemove', mockEvent(12, 10)))
    mockCanvas.dispatchEvent(new CustomEvent('mouseup', mockEvent(12, 12)))

    expect(pencil.contextDrawLine.mock.calls.length).toBe(3)
  })
})
