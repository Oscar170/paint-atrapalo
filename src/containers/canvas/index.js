// @flow
import { Canvas } from '../../ui'
import './index.css'
import appendList from '../../helpers/append-list'

export type CanvasBuilderDefinition = {
  names: string[],
  width: number,
  height: number,
}

const CanvasBuilder = ({ names, width, height }: CanvasBuilderDefinition) => {
  const domDiv = document.createElement('div')
  domDiv.className = 'canvas-container'
  domDiv.style.setProperty('width', `${width}px`)
  domDiv.style.setProperty('height', `${height}px`)

  return names
    .map(name => Canvas({ name, width, height }))
    .reduce(appendList, domDiv)
}

export default CanvasBuilder
