import CanvasBuilder from './index'

test('should create multiple canvas', () => {
  const result = CanvasBuilder({ names: ['a', 'b'], width: 20, height: 20 })
  const [canvasA, canvasB] = result.querySelectorAll('canvas')

  expect(result.className).toBe('canvas-container')
  expect(result.style.getPropertyValue('width')).toBe('20px')
  expect(result.style.getPropertyValue('height')).toBe('20px')
  expect(canvasA.className).toBe('a')
  expect(canvasB.className).toBe('b')
})
