import { Input, Color } from '../../ui'
import appendList from '../../helpers/append-list'
import './index.css'

const getColors = () => JSON.parse(localStorage.getItem('old-colors')) || []

const addColor = (color) => {
  const colors = [color, ...getColors()]
  localStorage.setItem('old-colors', JSON.stringify(
    colors.reduce((uniqueColors, colorToCheck) => {
      if (uniqueColors.includes(colorToCheck)) {
        return uniqueColors
      }
      return [...uniqueColors, colorToCheck]
    }, [])
      .slice(0, 10),
  ))
}

const ColorSelector = ({ onChange }) => {
  const domDiv = document.createElement('div')

  const inputColor = document.createElement('div')
  inputColor.appendChild(Input({
    type: 'color',
    onChange: (e) => {
      addColor(e.target.value)
      onChange(e.target.value)
    },
  }))

  const domOldColors = document.createElement('div')
  domOldColors.className = 'old-colors'

  const oldColors = getColors()

  domDiv.appendChild(inputColor)
  domDiv.appendChild(oldColors
    .map(color => Color({
      color,
      onClick: (newColor) => {
        addColor(newColor)
        onChange(newColor)
      },
    }))
    .reduce(appendList, domOldColors))

  return domDiv
}

export default ColorSelector
export {
  addColor,
  getColors,
}
