import ColorSelector, { addColor, getColors } from './index'

const clearStorage = () => localStorage.clear()

describe('aux functions for color', () => {
  beforeEach(clearStorage)

  test('should return empty list when don\'t have items in the storage', () => {
    const result = getColors()

    expect(result).toEqual([])
  })

  test('should return the stored colors', () => {
    addColor('#ff0000')
    const result = getColors()

    expect(result).toEqual(['#ff0000'])
  })

  test('only should save 10 items', () => {
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(addColor)

    expect(getColors()).toEqual([10, 9, 8, 7, 6, 5, 4, 3, 2, 1])
  })

  test('shouls remove repeated elements', () => {
    [0, 0].map(addColor)

    expect(getColors()).toEqual([0])
  })
})

describe('ColorSelector element', () => {
  beforeAll(clearStorage)
  afterAll(clearStorage)

  test('default creation without oldcolors', () => {
    const result = ColorSelector({})
    const [input, colors] = result.children

    expect(input.querySelector('input')).not.toBeNull()
    expect(colors.children.length).toBe(0)
  })

  test('creation with colors should create color elements', () => {
    ['red', 'blue'].map(addColor)

    const result = ColorSelector({})
    const [input, colors] = result.children

    expect(input.querySelector('input')).not.toBeNull()
    expect(colors.children.length).toBe(2)
  })

  test('when change the color should call the handler', () => {
    const onChange = jest.fn()
    const result = ColorSelector({ onChange })
    const inputElement = result.querySelector('input')

    inputElement.value = '#ff0000'
    inputElement.dispatchEvent(new Event('input'))
    const [firstCallParams] = onChange.mock.calls

    expect(firstCallParams).toEqual(['#ff0000'])
  })

  test('when click old color should call the handler', () => {
    localStorage.clear()
    const onChange = jest.fn()
    addColor('#ff0000')
    addColor('#0000ff')
    const result = ColorSelector({ onChange })

    const colors = getColors()
    const colorsDom = result.children[1]
    const randomClick = Math.floor(Math.random() * colors.length)

    colorsDom.children[randomClick].click()
    const [firstCallParams] = onChange.mock.calls

    expect(firstCallParams).toEqual([colors[randomClick]])
  })
})
