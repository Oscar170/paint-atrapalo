import { Line } from '../../ui'
import './index.css'
import appendList from '../../helpers/append-list'

const LineSelector = ({ sizes, onChange }) => {
  const domDiv = document.createElement('div')
  domDiv.className = 'line-selector'

  return sizes
    .sort((a, b) => a - b)
    .map(width => Line({ width, onClick: onChange }))
    .reduce(appendList, domDiv)
}
export default LineSelector
