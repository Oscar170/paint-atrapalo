import LineSelector from './index'

describe('should create line selector', () => {
  const result = LineSelector({ sizes: [5, 15, 10] })

  test('should be div element', () => {
    expect(result.tagName).toBe('DIV')
  })

  test('should add the lines sorted be size', () => {
    const [line5, line10, line15] = result.children
    expect(line5.style.getPropertyValue('height')).toBe('5px')
    expect(line10.style.getPropertyValue('height')).toBe('10px')
    expect(line15.style.getPropertyValue('height')).toBe('15px')
  })
})


test('should call the handler when click the element', () => {
  const onChange = jest.fn()
  const result = LineSelector({ sizes: [5, 15, 10], onChange })

  result.children[1].click()

  const [firstCallParams] = onChange.mock.calls

  expect(firstCallParams).toEqual([10])
})
