// @flow
import { Tool, ToolsMenu } from '../../ui'
import type { ToolDefinition } from '../../ui/tool'
import appendList from '../../helpers/append-list'

const MenuBuilder = (tools: ToolDefinition[]): HTMLElement | HTMLDivElement => tools
  .map(Tool)
  .reduce(appendList, ToolsMenu())

export default MenuBuilder
