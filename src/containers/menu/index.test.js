import MenuBuilder from './index'

test('should generate the menu with the tools', () => {
  const imageUrl = 'ssss'
  const action = () => {}

  const menu = MenuBuilder([
    { imageUrl, action },
    { imageUrl, action },
    { imageUrl, action },
  ])

  const tools = menu.querySelectorAll('.tool')

  expect(menu).not.toBeNull()
  expect(menu.className).toBe('tools-menu')
  expect(tools.length).toBe(3)
})
