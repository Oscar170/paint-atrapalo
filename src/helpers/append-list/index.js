// @flow
const appendList = (container: HTMLElement, item: HTMLElement): HTMLElement => {
  container.appendChild(item)
  return container
}

export default appendList
