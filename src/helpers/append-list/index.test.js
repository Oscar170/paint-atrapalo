import appendList from './index'

test('should add elementB to other elementA', () => {
  const elementA = document.createElement('div')
  const elementB = document.createElement('div')

  const result = appendList(elementA, elementB)
  const [child] = result.children

  expect(result).toBe(elementA)
  expect(child).toBe(elementB)
})
