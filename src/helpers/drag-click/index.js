
// @flow
import eventToPoint from '../point'
import type { Point } from '../point'

type Callback = (action: { type: string, move: { init: Point, finish: Point }}) => void | any

export default class DragClick {
  element: HTMLElement

  observers: Callback[]

  state: {
    init: Point,
    finish: Point,
  }

  constructor(element: HTMLElement) {
    this.element = element
    this.observers = []
  }

  init() {
    this.element.addEventListener('mousedown', this.initStream)
    this.element.addEventListener('mouseup', this.finishStream)
  }

  destroy() {
    this.observers = []
    this.element.removeEventListener('mousedown', this.initStream)
    this.element.removeEventListener('mousemove', this.updateStream)
    this.element.removeEventListener('mouseup', this.finishStream)
  }


  initStream = (e: MouseEvent) => {
    e.stopImmediatePropagation()
    this.initState(eventToPoint(e))
    this.notify('init')
    this.element.addEventListener('mousemove', this.updateStream)
  }

  finishStream = (e: MouseEvent) => {
    e.stopImmediatePropagation()
    this.updateState(eventToPoint(e))
    this.notify('finish')
    this.element.removeEventListener('mousemove', this.updateStream)
  }

  updateStream = (e: MouseEvent) => {
    e.stopImmediatePropagation()
    this.updateState(eventToPoint(e))
    this.notify('update')
  }

  initState = (newPoint: Point) => {
    this.state = {
      init: { ...newPoint, x: newPoint.x - 1 },
      finish: newPoint,
    }
  }

  updateState = (newPoint: Point) => {
    this.state = {
      init: this.state.finish,
      finish: newPoint,
    }
  }

  subscribe = (callback: Callback) => {
    this.observers = [...this.observers, callback]
  }

  unsubscribe = (callback: Callback) => {
    this.observers = this.observers.filter(observer => observer !== callback)
  }

  notify = (type: string) => {
    this.observers.forEach(callback => callback({ type, move: this.state }))
  }
}
