import DragClick from './index'

test('subscribe should add function to the stack', () => {
  const element = document.createElement('div')
  const dragClick = new DragClick(element)

  dragClick.subscribe(() => {})
  expect(dragClick.observers.length).toBe(1)
})

test('unsubscribe should remove the function from the stack', () => {
  const element = document.createElement('div')
  const dragClick = new DragClick(element)
  const mockFunction = () => {}
  dragClick.subscribe(mockFunction)
  dragClick.unsubscribe(mockFunction)

  expect(dragClick.observers.length).toBe(0)
})

test('notify should call the observers', () => {
  const callback = jest.fn()
  const element = document.createElement('div')
  const dragClick = new DragClick(element)

  dragClick.subscribe(callback)
  dragClick.notify('test')

  expect(callback).toBeCalled()
})

describe('update the state', () => {
  const element = document.createElement('div')
  const dragClick = new DragClick(element)

  test('initStream should initialize the state and call the notify', () => {
    const callback = jest.fn()
    dragClick.subscribe(callback)
    dragClick.initStream({
      stopImmediatePropagation: () => {},
      target: {
        getBoundingClientRect: () => ({ top: 0, left: 0 }),
      },
      pageX: 10,
      pageY: 10,
    })
    dragClick.unsubscribe(callback)

    const [call] = callback.mock.calls
    const [param] = call
    expect(param).toEqual({
      type: 'init',
      move: {
        init: { x: 9, y: 10 },
        finish: { x: 10, y: 10 },
      },
    })
  })

  test('updateStream should edit the state and call the notify', () => {
    const callback = jest.fn()
    dragClick.subscribe(callback)
    dragClick.updateStream({
      stopImmediatePropagation: () => {},
      target: {
        getBoundingClientRect: () => ({ top: 0, left: 0 }),
      },
      pageX: 12,
      pageY: 10,
    })
    dragClick.unsubscribe(callback)

    const [call] = callback.mock.calls
    const [param] = call
    expect(param).toEqual({
      type: 'update',
      move: {
        init: { x: 10, y: 10 },
        finish: { x: 12, y: 10 },
      },
    })
  })

  test('finishStream should edit the state and call the notify', () => {
    const callback = jest.fn()
    dragClick.subscribe(callback)
    dragClick.finishStream({
      stopImmediatePropagation: () => {},
      target: {
        getBoundingClientRect: () => ({ top: 0, left: 0 }),
      },
      pageX: 12,
      pageY: 12,
    })
    dragClick.unsubscribe(callback)

    const [call] = callback.mock.calls
    const [param] = call
    expect(param).toEqual({
      type: 'finish',
      move: {
        init: { x: 12, y: 10 },
        finish: { x: 12, y: 12 },
      },
    })
  })
})
