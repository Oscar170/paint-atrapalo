// @flow
export type Point = {
  x: number,
  y: number,
}
const eventToPoint = (e: MouseEvent): Point => {
  const { target, pageX, pageY } = e
  // flow-disable-next-line
  const { left, top } = target.getBoundingClientRect()
  return {
    x: pageX - left,
    y: pageY - top,
  }
}

export default eventToPoint
