import eventToPoint from './index'

// Mock
const clickCoords = { x: 200, y: 100 }
const elementPosition = { x: 100, y: 100 }
const event = {
  target: {
    getBoundingClientRect: () => ({ top: elementPosition.y, left: elementPosition.y }),
  },
  pageX: clickCoords.x,
  pageY: clickCoords.y,
}

test('should transform the mose event from dom element to point', () => {
  const point = eventToPoint(event)

  expect(point).toEqual({ x: 100, y: 0 })
})
