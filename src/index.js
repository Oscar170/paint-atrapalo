// @flow
import {
  CanvasBuilder,
  ColorSelector,
  LineSelector,
  MenuBuilder,
} from './containers'
import './index.css'
import DrawStore from './store/draw'
import Pencil from './actions/pencil'
import {
  Modal,
} from './ui'
import HistoryList from './actions/history'

const app: HTMLElement | null = document.querySelector('.app')

const canvasContainer = CanvasBuilder({
  names: ['main'],
  width: 540,
  height: 360,
})

const [main: HTMLCanvasElement] = canvasContainer.children

const store = new DrawStore(main, new HistoryList())
store.registerTool('pencil', Pencil)
store.changeTool('pencil')

const toolsMenu = MenuBuilder([{
  imageUrl: './assets/pencil.svg',
  action: () => store.changeTool('pencil'),
}, {
  imageUrl: './assets/paleta.svg',
  action: () => {
    if (app) app.appendChild(Modal({ title: 'Canviar color', content: ColorSelector({ onChange: (color) => { store.updateContext('strokeStyle', color) } }) }))
  },
}, {
  imageUrl: './assets/size.svg',
  action: () => {
    if (app) app.appendChild(Modal({ title: 'Canviar color', content: LineSelector({ sizes: [5, 10, 15, 20], onChange: (size) => { store.updateContext('lineWidth', size) } }) }))
  },
}])

if (app) {
  app.appendChild(toolsMenu)
  app.appendChild(canvasContainer)
}
