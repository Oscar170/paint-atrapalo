// @flow
import type { History } from '../../actions/history/history'

export interface ToolLifeCycle {
  constructor(canvas: HTMLCanvasElement, ctx: CanvasRenderingContext2D): void;
  onInit(): void;
  onDestroy(): void;
}

export default class DrawStore {
  canvas: HTMLCanvasElement

  ctx: CanvasRenderingContext2D

  history: History

  tools: { [string]: ToolLifeCycle }

  currentTool: string

  constructor(canvas: HTMLCanvasElement, history?: History) {
    this.canvas = canvas
    this.ctx = canvas.getContext('2d')
    this.tools = {}

    this.ctx.lineWidth = 5
    this.ctx.lineJoin = 'round'

    if (history) {
      this.history = history
      this.history.update(this.canvas.toDataURL())

      document.addEventListener('keydown', this.historyCallBack)
    }
  }

  registerTool = (name: string, Tool: Function) => {
    this.tools = {
      ...this.tools,
      [name]: new Tool(this.canvas, this.ctx, this.history),
    }
  }

  changeTool = (name: string) => {
    if (this.currentTool) {
      this.tools[this.currentTool].onDestroy()
    }

    this.currentTool = name
    this.tools[this.currentTool].onInit()
  }

  updateContext(prop: string, value: any) {
    // flow-disable-next-line
    this.ctx[prop] = value
  }

  historyCallBack = (e: KeyboardEvent) => {
    const { key, ctrlKey } = e
    if (ctrlKey && ['z', 'y'].includes(key)) {
      const p = key === 'z' ? this.history.undo() : this.history.redo()
      p.then((image) => {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        this.ctx.drawImage(image, 0, 0)
      })
    }
  }
}
