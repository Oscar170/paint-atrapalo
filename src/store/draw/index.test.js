import DrawStore from './index'

test('initialize DrawStore', () => {
  const getContext = jest.fn(() => ({}))
  const store = new DrawStore({ getContext })

  expect(store.ctx).not.toBeNull()
})


test('registerTool should store the instance', () => {
  const getContext = jest.fn(() => ({}))
  const store = new DrawStore({ getContext })
  store.registerTool('test', () => ({}))

  // eslint-disable-next-line
  expect(store.tools['test']).toEqual({})
})

test('registerTool should store the instance', () => {
  const getContext = jest.fn(() => ({}))
  const store = new DrawStore({ getContext })

  store.currentTool = 'test'
  store.registerTool('test', class Test {
      onInit = jest.fn()

      onDestroy = jest.fn()
  })

  store.changeTool('test')

  // eslint-disable-next-line
  expect(store.tools['test'].onInit).toBeCalled()
  // eslint-disable-next-line
  expect(store.tools['test'].onDestroy).toBeCalled()
})
