// @flow
export type CanvasDefinition = {
  name: string,
  width: number,
  height: number,
}

const Canvas = ({ name, width, height }: CanvasDefinition): HTMLCanvasElement => {
  const domCanvas = document.createElement('canvas')
  domCanvas.className = name
  domCanvas.width = width
  domCanvas.height = height

  return domCanvas
}

export default Canvas
