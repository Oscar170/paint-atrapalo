import Canvas from './index'

const result = Canvas({ name: 'test', width: 120, height: 100 })

test('should return canvas element', () => {
  expect(result.tagName).toBe('CANVAS')
})

test('should set the correct props', () => {
  const { className, width, height } = result

  expect(className).toBe('test')
  expect(width).toBe(120)
  expect(height).toBe(100)
})
