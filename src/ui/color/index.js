import './index.css'

const Color = ({ color, onClick } = {}) => {
  const domButton = document.createElement('button')
  domButton.className = 'color-ball'
  domButton.style.setProperty('background', color)

  if (onClick) {
    domButton.addEventListener('click', () => onClick(color))
  }

  return domButton
}

export default Color
