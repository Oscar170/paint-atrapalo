import Color from './index'

test('should return div element', () => {
  const result = Color({ color: '#ff0000' })

  expect(result.tagName).toBe('BUTTON')
})

test('should set the correct color', () => {
  const colorElement = Color({ color: '#ff0000' })
  const result = colorElement.style.getPropertyValue('background')

  // the value is parsed to rgb by default
  expect(result).toBe('rgb(255, 0, 0)')
})

test('should call the handler when click the element', () => {
  const onClick = jest.fn()
  const colorElement = Color({ color: '#ff0000', onClick })

  colorElement.click()
  const [firstCallParams] = onClick.mock.calls

  expect(firstCallParams).toEqual(['#ff0000'])
})
