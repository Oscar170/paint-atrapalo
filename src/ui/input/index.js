const Input = ({ type = 'text', onChange } = {}) => {
  const domInput = document.createElement('input')
  domInput.type = type
  if (onChange) {
    domInput.addEventListener('input', onChange)
  }

  return domInput
}

export default Input
