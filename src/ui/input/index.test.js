import Input from './index'

test('should create input element', () => {
  const result = Input({})

  expect(result.tagName).toBe('INPUT')
})

test('should create text input by default', () => {
  const result = Input({})

  expect(result.type).toBe('text')
})

test('should create input whit the correct type', () => {
  const result = Input({ type: 'color' })

  expect(result.type).toBe('color')
})

test('should call the handler when update the value', () => {
  const onChange = jest.fn()
  const inputElement = Input({ type: 'color', onChange })

  inputElement.value = '#ff0000'
  inputElement.dispatchEvent(new Event('input'))

  const [firstCallParams] = onChange.mock.calls
  const [event] = firstCallParams

  expect(event.target.value).toBe('#ff0000')
})
