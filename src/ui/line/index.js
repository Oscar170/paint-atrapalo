import './index.css'

const Line = ({ width, onClick }) => {
  const domDiv = document.createElement('div')
  domDiv.className = 'line'
  domDiv.style.setProperty('height', `${width}px`)

  if (onClick) {
    domDiv.addEventListener('click', () => onClick(width))
  }

  return domDiv
}

export default Line
