import Line from './index'

test('should return div element', () => {
  const result = Line({ width: 200 })

  expect(result.tagName).toBe('DIV')
})

test('should set the correct height', () => {
  const lineElement = Line({ width: 200 })
  const result = lineElement.style.getPropertyValue('height')

  expect(result).toBe('200px')
})

test('should call the handler when click the element', () => {
  const onClick = jest.fn()
  const lineElement = Line({ width: 200, onClick })

  lineElement.click()
  const [firstCallParams] = onClick.mock.calls

  expect(firstCallParams).toEqual([200])
})
