import './index.css'

const Modal = ({ title, onClose, content = '' } = {}) => {
  const domDiv = document.createElement('div')
  domDiv.className = 'modal'

  const domBackground = document.createElement('div')
  domBackground.className = 'modal-background'

  domBackground.appendChild(domDiv)

  const domHeader = document.createElement('div')
  domHeader.className = 'modal-header'

  if (title) {
    const domSpan = document.createElement('span')
    domSpan.innerText = title

    domHeader.appendChild(domSpan)
  }

  const domButton = document.createElement('button')
  domButton.className = 'close'
  domButton.innerText = '✖'
  domButton.addEventListener('click', () => {
    if (onClose) onClose()
    domBackground.parentNode.removeChild(domBackground)
  })

  domHeader.appendChild(domButton)

  const domContent = document.createElement('div')
  domContent.className = 'modal-content'

  if (typeof content === 'string') {
    domContent.innerHTML = content
  } else {
    domContent.appendChild(content)
  }

  domDiv.appendChild(domHeader)
  domDiv.appendChild(domContent)

  return domBackground
}

export default Modal
