import Modal from './index'

describe('default creation of the modal', () => {
  const result = Modal({})
  const [modal] = result.children

  test('should be div element', () => {
    expect(modal.tagName).toBe('DIV')
  })

  test('should have two elements: header and content', () => {
    const [header, content] = modal.children

    expect(header.className).toBe('modal-header')
    expect(content.className).toBe('modal-content')
  })
})

describe('close action', () => {
  test('should remove the element when click on close', () => {
    const container = document.createElement('div')
    const modal = Modal({})
    container.appendChild(modal)

    modal.querySelector('.close').click()
    expect(container.children.length).toBe(0)
  })

  test('should call the callback close function', () => {
    const onClose = jest.fn()
    const container = document.createElement('div')
    const modal = Modal({ onClose })
    container.appendChild(modal)

    modal.querySelector('.close').click()

    expect(onClose.mock.calls.length).toBe(1)
  })
})

describe('add content to the modal', () => {
  test('should add the title', () => {
    const result = Modal({ title: 'test' })
    const title = result.querySelector('.modal-header > span')

    expect(title.innerText).toBe('test')
  })
  test('should add the html content', () => {
    const result = Modal({ content: document.createElement('hr') })
    const content = result.querySelector('.modal-content')
    const [value] = content.children

    expect(value.tagName).toBe('HR')
  })
  test('should add the string content', () => {
    const result = Modal({ content: '<h1>Test</h1>' })
    const content = result.querySelector('.modal-content')
    const [value] = content.children

    expect(value.tagName).toBe('H1')
  })
})
