// @flow
import './index.css'

export type ToolDefinition = {
  imageUrl: string,
  description?: string,
  action: () => void,
}

const Tool = ({ imageUrl, description, action }: ToolDefinition): HTMLButtonElement => {
  const domButton = document.createElement('button')
  domButton.className = 'tool'

  domButton.addEventListener('click', action)

  const domImage = document.createElement('img')
  domImage.src = imageUrl

  domButton.appendChild(domImage)

  if (description) {
    const domSpan = document.createElement('span')
    domSpan.innerHTML = description

    domButton.appendChild(domSpan)
  }

  return domButton
}

export default Tool
