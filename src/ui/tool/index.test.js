import Tool from './index'

test('should return button element', () => {
  const result = Tool({ imageUrl: 'ssss', action: () => {} })

  expect(result.tagName).toBe('BUTTON')
})

test('should add the image element', () => {
  const result = Tool({ imageUrl: 'ssss', action: () => {} })
  const image = result.querySelector('img')

  expect(image.src).toBe('http://localhost/ssss')
})

test('on click button should call action callback', () => {
  const action = jest.fn()
  const result = Tool({ imageUrl: 'ssss', action })
  result.click()

  expect(action.mock.calls.length).toBe(1)
})

test('dhould add the description', () => {
  const result = Tool({ imageUrl: 'ssss', description: 'test', action: () => {} })
  const description = result.querySelector('span')

  expect(description.tagName).toBe('SPAN')
})
