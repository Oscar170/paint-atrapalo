// @flow
import './index.css'

const ToolsMenu = (): HTMLDivElement => {
  const domDiv = document.createElement('div')
  domDiv.className = 'tools-menu'

  return domDiv
}

export default ToolsMenu
