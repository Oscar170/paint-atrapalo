import ToolsMenu from './index'

describe('should have to create the tool menu', () => {
  const result = ToolsMenu()

  test('should be div element', () => {
    expect(result.tagName).toBe('DIV')
  })

  test('should have the correct class name', () => {
    expect(result.className).toBe('tools-menu')
  })
})
