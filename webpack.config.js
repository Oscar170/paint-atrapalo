const HtmlWebpackPLugin = require('html-webpack-plugin')

module.exports = {
  entry: `${__dirname}/src`,
  output: {
    path: `${__dirname}/dist`,
    filename: 'bundle.js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [{
          loader: 'babel-loader'
        }],
        exclude: /node_modules/
      }, {
        test: /\.css$/,
        use: [{
            loader: "style-loader" // creates style nodes from JS strings
        }, {
            loader: "css-loader" // translates CSS into CommonJS
        }]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPLugin({
      template: `${__dirname}/public/index.html`,
      inject: 'body'
    })
  ],
  devServer: {
    contentBase: './public',
    port: 3000
  }
}